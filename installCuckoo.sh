#!/bin/bash
# A simple script to install cuckoo sandbox on ubuntu
# Khalid Alsuwaiyel
# Nov 4, 2015


echo "*****************************************************************"
echo "*****************************************************************"
echo "*************First Step: Install Dependency Pckages**************"
echo "*****************************************************************"
echo "*****************************************************************"
echo ""
echo ""
#Adding necessary repository to sources list
if [[ $(find /etc/apt/ -name *.list -type f -exec grep "vivid" {} \; -quit) = *vivid* ]]; then
	echo "Vivid universe already enabled"
else
	echo "Adding vivid universe repository to sources list"
	echo "deb http://archive.ubuntu.com/ubuntu/ vivid universe" \
 	  >>  "/etc/apt/sources.list"
fi

#list of packages
pkglist="python
python-sqlalchemy
python-bson
python-jinja2
python-pymongo
python-gridfs
python-libvirt
bridge-utils
python-pyrex
python-bottle
python-pefile
python-dpkt
git
libpcre3
libpcre3-dev
python-pip
python-dev
build-essential
tcpdump
libffi-dev
libssl-dev"

#update
echo -n "Updating system..."
if (apt-get -y update > /dev/null) ; then
	echo "+ + + Succeeded + + +"
else
	echo "----FAILED----"
fi

#install packages
for pkg in ${pkglist}
do
    if (dpkg --list | awk '{print $2}' | egrep "^${pkg}$" > /dev/null) ; then
        echo "${pkg} is already installed"
    else
        echo -n "Trying to install ${pkg}..."
        if (apt-get -y install ${pkg} > /dev/null) ; then
            echo "+ + + Succeeded + + +"
        else
            echo "----FAILED----"
            exit 1
        fi
    fi
done

echo -n "Upgrading pip..."
if(pip install --upgrade pip > /dev/null) ; then
	echo "+ + + Succeeded + + +"
else
        echo "----FAILED----"
fi

echo "Installing requests[secuirt]..."
if(pip install requests[security] > /dev/null) ; then
        echo "+ + + Succeeded + + +"
else
        echo "----FAILED----"
fi

echo -n "upgrading virtualenv..."
if(pip install --upgrade virtualenv > /dev/null) ; then
        echo "+ + + Succeeded + + +"
else
        echo "----FAILED----"
fi

pippkglist="pyopenssl
ndg-httpsclient
pyasn1
python-magic
jinja2
pymongo
bottle
pefile
cybox
maec
django
chardet"

#install pip packages
for pippkg in ${pippkglist}
do
    if (pip list | awk '{print $1}' | egrep --ignore-case "^${pippkg}$" > /dev/null) ; then
        echo "${pippkg} is already installed"
    else
        echo -n "Trying to install ${pippkg}..."
        if (pip install ${pippkg} > /dev/null) ; then
            echo "+ + + Succeeded + + +"
        else
            echo "----FAILED----"
            exit 1
        fi
    fi
done

setcap cap_net_raw,cap_net_admin=eip /usr/sbin/tcpdump
getcap /usr/sbin/tcpdump

if [ -f "/usr/local/bin/ssdeep" ]; then
	echo "ssdeep is already installed"
else
	echo -n "Trying to install ssdeep..."
	wget http://sourceforge.net/projects/ssdeep/files/ssdeep-2.12/ssdeep-2.12.tar.gz > /dev/null
	tar xvzf ssdeep-2.12.tar.gz > /dev/null
	cd ssdeep-2.12/ 
	./configure > /dev/null
	make > /dev/null
	make install > /dev/null
	cd ..
	echo "+ + + Succeeded + + +"
fi

PAT="$(pwd)/pydeep"
if [ -d $PAT ]; then
	echo "pydeep is already installed"
else
	echo -n "Trying to install pydeep..."
	git clone https://github.com/kbandla/pydeep > /dev/null
	cd pydeep > /dev/null
	python setup.py build > /dev/null
	python setup.py install > /dev/null
	cd ..
	echo "+ + + Succeeded + + +"
fi

if [ -f "/usr/local/bin/yara" ]; then
	echo "yara is already installed"
else
	echo -n "Trying to install yara..."
	wget https://yara-project.googlecode.com/files/yara-1.7.tar.gz -q -o /dev/null
	tar xvzf yara-1.7.tar.gz > /dev/null
	cd yara-1.7/ 
	./configure > /dev/null
	make > /dev/null
	make install > /dev/null
	echo "/usr/local/lib" >> /etc/ld.so.conf
	ldconfig
	cd ..
	echo "+ + + Succeeded + + +"
fi

PAT="$(pwd)/yara-python-1.7"
if [ -d $PAT ]; then
	echo "yara-python is already installed"
else
	echo -n "Trying to install yara-python..."
	wget https://yara-project.googlecode.com/files/yara-python-1.7.tar.gz -q -o /dev/null
	tar xvzf yara-python-1.7.tar.gz > /dev/null
	cd yara-python-1.7/
	python setup.py build > /dev/null
	python setup.py install > /dev/null
	cd ..
	echo "+ + + Succeeded + + +"
fi

PAT="$(pwd)/distorm3"
if [ -d $PAT ]; then
	echo "distorm3 is already installed"
else
	echo -n "Trying to install distorm3..."
	wget https://distorm.googlecode.com/files/distorm3.zip -q -o /dev/null
	unzip distorm3.zip > /dev/null
	cd distorm3/
	python setup.py build > /dev/null
	python setup.py install > /dev/null
	cd ..
	echo "+ + + Succeeded + + +"
fi

PAT="$(pwd)/volatility-2.3.1"
if [ -d $PAT ]; then
	echo "volatility is already installed"
else
	echo -n "Trying to install volatility..."
	wget https://volatility.googlecode.com/files/volatility-2.3.1.tar.gz -q -o /dev/null
	tar xvzf volatility-2.3.1.tar.gz > /dev/null
	cd volatility-2.3.1/
	python setup.py build > /dev/null
	python setup.py install > /dev/null
	cd ..
	echo "+ + + Succeeded + + +"
fi
